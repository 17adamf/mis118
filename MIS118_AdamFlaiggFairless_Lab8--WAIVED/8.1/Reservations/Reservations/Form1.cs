using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reservations
{
    public partial class Form1 : Form
    {
        DateTime arrivalDate;
        DateTime departDate;
        DateTime currentDate;
        int nightsCount;
        decimal nightlyCost = 120.00m;
        decimal nightlyFriSatCost = 150.00m;
        decimal totalPrice = 0.00m;
        decimal avgPrice;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            currentDate = System.DateTime.Today;
            arrivalDate = currentDate;
            departDate = currentDate.AddDays(3);

            txtArrivalDate.Text = arrivalDate.ToString("MMddyyyy");
            txtDepartureDate.Text = departDate.ToString("MMddyyyy");
            nightsCount = Convert.ToInt32((departDate - arrivalDate).TotalDays);
            txtNights.Text = nightsCount.ToString();
        }
        public bool IsValidData()
        {
            if (IsPresent(txtArrivalDate, "Arrival Date") &&
                IsPresent(txtDepartureDate, "Departure Date") &&
                IsDateTime(txtArrivalDate, "Arrival Date") &&
                IsDateTime(txtDepartureDate, "Departure Date") &&
                IsWithinRange(txtArrivalDate, "Arrival Date", arrivalDate, departDate) &&
                IsWithinRange(txtDepartureDate, "Departure Date", arrivalDate, departDate)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (IsValidData())
            {
                DateTime tempDate = arrivalDate;
                while (tempDate < departDate)
                {
                    if (tempDate.DayOfWeek == DayOfWeek.Friday || tempDate.DayOfWeek == DayOfWeek.Saturday)
                    {
                        totalPrice += nightlyFriSatCost;
                    }
                    else
                    {
                        totalPrice += nightlyCost;
                    }
                    tempDate.AddDays(1);
                }
                avgPrice = (totalPrice / nightsCount);

                txtTotalPrice.Text = totalPrice.ToString();
                txtAvgPrice.Text = avgPrice.ToString();

            }
            else
            {
                MessageBox.Show("Fix specified fields");
            }
        }

        public bool IsPresent(TextBox textBox, string name)
        {
            if (textBox.Text == "")
            {
                MessageBox.Show(name + " is a required field.", "Entry Error");
                textBox.Focus();
                return false;
            }
            return true;
        }

        public bool IsDateTime(TextBox textBox, string name)
        {
            try
            {
                arrivalDate = Convert.ToDateTime(txtArrivalDate.Text);
                departDate = Convert.ToDateTime(txtDepartureDate.Text);

                return true;
            }
            catch (Exception err)
            {

                MessageBox.Show(name + " is not a date", err.Message);
                return false;
            }

        }

        public bool IsWithinRange(TextBox textBox, string name,
            DateTime min, DateTime max)
        {
            try
            {
                DateTime dateToCheck = Convert.ToDateTime(textBox);
                if (dateToCheck >= min && dateToCheck <= max)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show(name + " is outside range");
                    return false;
                }
            }
            catch
            {
                MessageBox.Show(name + " is not a date");
                return false;
            }
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}