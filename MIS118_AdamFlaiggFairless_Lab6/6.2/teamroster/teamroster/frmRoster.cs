/*Class Name: frmScoreCalc
 * Author: Adam Flaigg-Fairless
 * Date: 11/12/2018
 * Lab: Lab 6 Part 2
 * Class Description: Takes user input for a team size, creates an array based on that size. 
 *                    Allows user to add members to the team array, and displays them in a 
 *                    list box on the left.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace teamroster
{
    public partial class frmRoster : Form
    {
        //create the globals
        int teamSize = 0;
        int selectedIndex = 0;
        string[] teamArray;

        public frmRoster()
        {
            InitializeComponent();
        }
        private void frmRoster_Load(object sender, EventArgs e)
        {

        }
        private void btnSubmitSize_Click(object sender, EventArgs e)
        {
            //convert the team size to avoid decimals
            teamSize = Convert.ToInt32(nudTeamSize.Value);
            //runs the create method
            createTeamArray(teamSize);
            //disables the buttons while team is created
            nudTeamSize.Enabled = false;
            btnSubmitSize.Enabled = false;
            //populate the array with open slots, so there is something in the slots to be selected
            for (int i = 0; i < teamSize; i++)
            {
                teamArray[i] = "";
            }
            //updates the teamsize info
            updateInfo();
            //enables member adding
            btnSubmitMember.Enabled = true;
        }



        private void btnSubmitMember_Click(object sender, EventArgs e)
        {
            //runs the addmember method using the current inputted text
            addMember(txtName.Text);
            //clear the input
            txtName.Text = "";
        }

        public void btnClear_Click(object sender, EventArgs e)
        {
            //clears the listbox
            liboxRosterMembers.Items.Clear();
            //sets all array items to blank
            for (int i = 0; i < teamSize; i++)
            {
                teamArray[i] = "";
            }
            //resets selected index
            selectedIndex = 0;
            //resets the name box
            txtName.Text = "";
            //flips button controls back to team size creation mode
            nudTeamSize.Enabled = true;
            btnSubmitSize.Enabled = true;
            btnSubmitMember.Enabled = false;
        }

        private void updateInfo()
        {
            //prints the total members and open slots based on the selected index
            lblTotals.Text = "Total Team Members: " + selectedIndex.ToString() + "\n\n" +
                             "Spots Open: " + (teamSize - selectedIndex).ToString();
        }
        public void createTeamArray(int size)
        {
            //creates the array based on the team size supplied
            teamArray = new string[size];
        }
        private void addMember(string name)
        {
            //checks to make sure we're within the team size limit
            if (selectedIndex < teamArray.Length)
            {
                //insert the supplied member name at current index
                teamArray[selectedIndex] = name;
                //add the name to the list box
                liboxRosterMembers.Items.Add(teamArray[selectedIndex]);
                //increment index
                selectedIndex++;
                //updates the totals
                updateInfo();
                //reselects the name input for user QOL; makes inputting data quicker
                txtName.Select();
            }
            //if we have reached our team size, alert the user that the roster is full
            else if (selectedIndex <= teamArray.Length)
            {
                MessageBox.Show("Maximum Team Size Reached", "Max Team Size", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
