namespace teamroster
{
    partial class frmRoster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.liboxRosterMembers = new System.Windows.Forms.ListBox();
            this.nudTeamSize = new System.Windows.Forms.NumericUpDown();
            this.btnSubmitSize = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnSubmitMember = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblTotals = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudTeamSize)).BeginInit();
            this.SuspendLayout();
            // 
            // liboxRosterMembers
            // 
            this.liboxRosterMembers.FormattingEnabled = true;
            this.liboxRosterMembers.Location = new System.Drawing.Point(12, 25);
            this.liboxRosterMembers.Name = "liboxRosterMembers";
            this.liboxRosterMembers.Size = new System.Drawing.Size(149, 472);
            this.liboxRosterMembers.TabIndex = 0;
            // 
            // nudTeamSize
            // 
            this.nudTeamSize.Location = new System.Drawing.Point(164, 25);
            this.nudTeamSize.Name = "nudTeamSize";
            this.nudTeamSize.Size = new System.Drawing.Size(120, 20);
            this.nudTeamSize.TabIndex = 1;
            // 
            // btnSubmitSize
            // 
            this.btnSubmitSize.Location = new System.Drawing.Point(293, 25);
            this.btnSubmitSize.Name = "btnSubmitSize";
            this.btnSubmitSize.Size = new System.Drawing.Size(110, 23);
            this.btnSubmitSize.TabIndex = 2;
            this.btnSubmitSize.Text = "Confirm Team Size";
            this.btnSubmitSize.UseVisualStyleBackColor = true;
            this.btnSubmitSize.Click += new System.EventHandler(this.btnSubmitSize_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(167, 81);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(120, 20);
            this.txtName.TabIndex = 3;
            // 
            // btnSubmitMember
            // 
            this.btnSubmitMember.Enabled = false;
            this.btnSubmitMember.Location = new System.Drawing.Point(293, 78);
            this.btnSubmitMember.Name = "btnSubmitMember";
            this.btnSubmitMember.Size = new System.Drawing.Size(110, 23);
            this.btnSubmitMember.TabIndex = 4;
            this.btnSubmitMember.Text = "Add To Team";
            this.btnSubmitMember.UseVisualStyleBackColor = true;
            this.btnSubmitMember.Click += new System.EventHandler(this.btnSubmitMember_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(12, 503);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(149, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear Roster";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblTotals
            // 
            this.lblTotals.AutoSize = true;
            this.lblTotals.Location = new System.Drawing.Point(168, 156);
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Size = new System.Drawing.Size(116, 39);
            this.lblTotals.TabIndex = 6;
            this.lblTotals.Text = "Total Team Members: -\r\n\r\nSpots Open: -";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Team Members";
            // 
            // frmRoster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 538);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTotals);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSubmitMember);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.btnSubmitSize);
            this.Controls.Add(this.nudTeamSize);
            this.Controls.Add(this.liboxRosterMembers);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmRoster";
            this.Text = "Team Roster";
            this.Load += new System.EventHandler(this.frmRoster_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudTeamSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox liboxRosterMembers;
        private System.Windows.Forms.NumericUpDown nudTeamSize;
        private System.Windows.Forms.Button btnSubmitSize;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnSubmitMember;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblTotals;
        private System.Windows.Forms.Label label1;
    }
}

