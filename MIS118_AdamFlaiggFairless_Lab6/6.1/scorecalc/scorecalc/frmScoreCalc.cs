/*Class Name: frmScoreCalc
 * Author: Adam Flaigg-Fairless
 * Date: 11/12/2018
 * Lab: Lab 6 Part 1
 * Class Description: takes score inputs, adds them to an array, and can display the inputs. Totals inputs and displays them on the form
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace scorecalc
{
    public partial class frmScoreCalc : Form
    {
        int selectedIndex = 0;
        int[] scoreArray = new int[20];

        public frmScoreCalc()
        {
            InitializeComponent();
        }

        private void frmScoreCalc_Load(object sender, EventArgs e)
        {

        }
        private void btnDisplay_Click(object sender, EventArgs e)
        {
            displayScores();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearForm();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //make sure we are within our 20 limit
            if (selectedIndex < 20)
            {
                try
                {
                    //convert the input
                    int convertedScoreInput = Convert.ToInt32(nudScoreInput.Value);
                    //store it in the current selected index
                    scoreArray[selectedIndex] = convertedScoreInput;
                    //increment the index
                    selectedIndex++;
                    //update the form
                    updateForm();
                }
                catch (Exception err)
                {
                    //throw exception if input goes wrong
                    MessageBox.Show(err.StackTrace, err.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            //if we've reached the max array size, alert the user
            else if (selectedIndex >= 20)
            {
                MessageBox.Show("Maximum scores entered", "Array Full", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void updateForm()
        {
            //run a for loop to add together all the items in the array
            int total = 0;
            for (int i = 0; i < scoreArray.Length; i++)
            {
                total += scoreArray[i];
            }
            //send total to form
            txtScoreTotal.Text = total.ToString();
            //send selected index as array size to form
            txtCount.Text = selectedIndex.ToString();
            //calculate the average and send to form
            txtAvg.Text = (total / selectedIndex).ToString();
        }
        private void clearForm()
        {
            //reset all vars and displayed items to nothing
            selectedIndex = 0;
            Array.Clear(scoreArray, 0, scoreArray.Length);
            txtAvg.Text = "";
            txtCount.Text = "";
            txtScoreTotal.Text = "";
            nudScoreInput.Value = 0;
        }
        private void displayScores()
        {
            //sort the array 
            Array.Sort(scoreArray);
            string output = "";
            for (int i = 0; i < scoreArray.Length; i++)
            {
                if (scoreArray[i].ToString() == "0")
                {
                    //do nothing, item is a 0
                }
                else
                {
                    //add the item in current index to the output string
                    output += scoreArray[i].ToString() + "\n";
                }

            }
            MessageBox.Show(output, "Sorted Scores");
        }


    }
}
