/*Class Name: frmStudentGrade
 * Author: Adam Flaigg-Fairless
 * Date: 10/25/18
 * Lab: Lab 4 Part 1
 * Class Description: takes an input number and determines student grade with a letter
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace studentgrade
{
    public partial class frmStudentGrade : Form
    {
        public frmStudentGrade()
        {
            InitializeComponent();
        }


        private void nudInput_ValueChanged(object sender, EventArgs e)
        {
            //take the input number and convert it to int, changes input to be rounded
            //if, by some chance, you manage to put a string into the numericUpDown, it lets you know
            int inputNumber = Convert.ToInt32(nudInput.Value);
            nudInput.Value = inputNumber;
            if (validateInput(inputNumber))
            {
                determineGrade(inputNumber);
            }
            else
            {
                MessageBox.Show("Somehow you managed to enter something that isn't a number into a numericUpDown. I'm proud of you", "Impossible Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void determineGrade(int inputNumber)
        {
            //sets the letter grade based on the grade percentage
            string letterGrade = "";


            if (inputNumber < 60)
            {
                letterGrade = "F";
            }
            else if (inputNumber >= 60 && inputNumber < 70)
            {
                letterGrade = "D";
            }
            else if (inputNumber >= 70 && inputNumber < 80)
            {
                letterGrade = "C";
            }
            else if (inputNumber >= 80 && inputNumber < 90)
            {
                letterGrade = "B";
            }
            else if (inputNumber >= 90)
            {
                letterGrade = "A";
            }
            //run the printOutput with the updated info
            printOutput(inputNumber, letterGrade);


        }

        private void printOutput(int inputNumber, string letterGrade)
        {
            //cat the grade percent and the letter grade into the text for the output
            lblOutput.Text = ("Grade Percentage: " + inputNumber.ToString() + ", Letter Grade: " + letterGrade);
        }

        private bool validateInput(Int32 inputNumber)
        {
            //make sure the input is, infact, an int which it should always be
            if (inputNumber is Int32)
            {
                return true;
            }
            return false;
        }
    }
}
