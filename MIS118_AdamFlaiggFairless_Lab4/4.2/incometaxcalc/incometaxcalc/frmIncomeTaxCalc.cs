/*Class Name: frmIncomeTaxCalc
 * Author: Adam Flaigg-Fairless
 * Date: 10/25/18
 * Lab: Lab 4 Part 2
 * Class Description: takes an input number and determines tax amount owed
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace incometaxcalc
{
    public partial class frmIncomeTaxCalc : Form
    {
        decimal outputNumber = 0;

        public frmIncomeTaxCalc()
        {
            InitializeComponent();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            //on click, run the update function
            decimal inputNumber = nudInput.Value;
            updateTaxOwed(inputNumber);
        }

        private void updateTaxOwed(decimal inputNumber)
        {
            //determine the tax bracket based on the range, remove the excess amount, and calculate the percent owed + the base amount
            if (inputNumber < 9225)
            {
                outputNumber = (Decimal.Multiply(inputNumber, 0.1m));
            }
            else if (inputNumber >= 9225 && inputNumber < 37450)
            {
                inputNumber -= 9225;
                outputNumber = (Decimal.Multiply(inputNumber, 0.15m));
                outputNumber += 922.50m;
            }
            else if (inputNumber >= 37450 && inputNumber < 90750)
            {
                inputNumber -= 37450;
                outputNumber = (Decimal.Multiply(inputNumber, 0.25m));
                outputNumber += 5156.25m;
            }
            else if (inputNumber >= 90750 && inputNumber < 189300)
            {
                inputNumber -= 90750;
                outputNumber = (Decimal.Multiply(inputNumber, 0.28m));
                outputNumber += 18481.25m;
            }
            else if (inputNumber >= 189300 && inputNumber < 411500)
            {
                inputNumber -= 189300;
                outputNumber = (Decimal.Multiply(inputNumber, 0.33m));
                outputNumber += 46075.25m;
            }
            else if (inputNumber >= 411500 && inputNumber < 413200)
            {
                inputNumber -= 411500;
                outputNumber = (Decimal.Multiply(inputNumber, 0.35m));
                outputNumber += 119401.25m;
            }
            else if (inputNumber >= 413200)
            {
                inputNumber -= 413200;
                outputNumber = (Decimal.Multiply(inputNumber, 0.396m));
                outputNumber += 1199625;
            }
            else
            {
                outputNumber = 0;
            }

            txtOutput.Text = Math.Round(outputNumber, 2).ToString();
            outputNumber = 0;
            nudInput.Value = 0;
            inputNumber = 0;
        }


    }
}
