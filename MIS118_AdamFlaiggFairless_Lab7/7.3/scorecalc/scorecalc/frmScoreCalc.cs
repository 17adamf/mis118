/*Class Name: frmScoreCalc
 * Author: Adam Flaigg-Fairless
 * Date: 11/12/2018
 * Lab: Lab 7 Part 3
 * Class Description: takes score inputs, adds them to a list, and can display the inputs. Totals inputs and displays them on the form
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace scorecalc
{
    public partial class frmScoreCalc : Form
    {

        List<int> scoreList = new List<int>();

        public frmScoreCalc()
        {
            InitializeComponent();
        }

        private void frmScoreCalc_Load(object sender, EventArgs e)
        {

        }
        private void btnDisplay_Click(object sender, EventArgs e)
        {
            displayScores();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearForm();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                //convert the input
                int convertedScoreInput = Convert.ToInt32(nudScoreInput.Value);
                scoreList.Add(convertedScoreInput);
                //update the form
                updateForm();
            }
            catch (Exception err)
            {
                //throw exception if input goes wrong
                MessageBox.Show(err.StackTrace, err.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateForm()
        {
            //run a for loop to add together all the items in the array
            int total = 0;
            for (int i = 0; i < scoreList.Count; i++)
            {
                total += scoreList[i];
            }
            //send total to form
            txtScoreTotal.Text = total.ToString();
            //send selected index as array size to form
            txtCount.Text = scoreList.Count.ToString();
            //calculate the average and send to form
            txtAvg.Text = (total / scoreList.Count).ToString();
        }
        private void clearForm()
        {
            //reset all vars and displayed items to nothing
            scoreList.Clear();
            txtAvg.Text = "";
            txtCount.Text = "";
            txtScoreTotal.Text = "";
            nudScoreInput.Value = 0;
        }
        private void displayScores()
        {
            //sort the array 
            scoreList.Sort();
            string output = "";
            for (int i = 0; i < scoreList.Count; i++)
            {
                if (scoreList[i].ToString() == "0")
                {
                    //do nothing, item is a 0
                }
                else
                {
                    //add the item in current index to the output string
                    output += scoreList[i].ToString() + "\n";
                }

            }
            MessageBox.Show(output, "Sorted Scores");
        }


    }
}
