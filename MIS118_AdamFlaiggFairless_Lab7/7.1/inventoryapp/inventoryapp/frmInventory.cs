/*Class Name: frmScoreCalc
 * Author: Adam Flaigg-Fairless
 * Date: 11/12/2018
 * Lab: Lab 7 Part 1&2
 * Class Description: Creates a 2D array, and allows the user to change the quantity of the items in the array.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace inventoryapp
{
    public partial class frmInventory : Form
    {
        int selectedIndex;
        public string[,] strInvItems = new string[,]
            {
                {"DS35Oak", "Oak Dining Room Set", "3299.89", "4\n"},
                {"DS12Maple", "Maple Dining Room Set", "1899.99", "5\n"}
            };
        public class Globals
        {


        }

        public frmInventory()
        {
            InitializeComponent();

        }

        private void frmInventory_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < strInvItems.GetLength(0); i++)
            {
                cbProdCode.Items.Add(strInvItems[i, 0]);
            }
            cbProdCode.SelectedIndex = 0;
        }

        private void cbProdCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedIndex = cbProdCode.SelectedIndex;
        }

        private void btnQuantity_Click(object sender, EventArgs e)
        {
            strInvItems[selectedIndex, 3] = nudQuantity.Value.ToString() + "\n";
            MessageBox.Show("Adjusted " + strInvItems[selectedIndex, 0] + " to " + strInvItems[selectedIndex, 3], "Updated Item");
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            string output = "";
            foreach (string str in strInvItems)
            {
                output += str + " ";
            }

            MessageBox.Show(output, "Output", MessageBoxButtons.OK);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


    }
}
