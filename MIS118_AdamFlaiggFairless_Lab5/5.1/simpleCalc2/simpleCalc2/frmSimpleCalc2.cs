/*Class Name: frmSimpleCalc2
 * Author: Adam Flaigg-Fairless
 * Date: 10/25/18
 * Lab: Lab 5 Part 1
 * Class Description: Takes two numbers and a user-inputted operator and performs the calculation
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace simpleCalc2
{
    public partial class frmSimpleCalc2 : Form
    {
        public frmSimpleCalc2()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtResult.Enabled = false;
            nudNumber1.Text = "";
            nudNumber2.Text = "";
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            decimal number1 = nudNumber1.Value;
            decimal number2 = nudNumber2.Value;
            string operatorString = txtOperator.Text;
            try
            {
                calculateResult(number1, number2, operatorString);
            }
            catch (DivideByZeroException)
            {
                MessageBox.Show("Enter a non-zero in the Number 2 slot", "Cannot Divide by 0", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (OverflowException err)
            {
                MessageBox.Show(err.Message + " " + err.StackTrace, "Overflow Exception", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (FormatException err)
            {
                MessageBox.Show(err.Message + " " + err.StackTrace, "Format Exception", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void calculateResult(decimal operant1, decimal operand2, string operatorString)
        {
            decimal result = 0;
            //determine the operator, run the calculation, and output to the textbox
            switch (operatorString)
            {
                case "/":
                case "\\":
                    //check both slashes incase user puts in a back slash
                    //operator is division
                    result = (operant1 / operand2);
                    break;
                case "*":
                    //operator is multi
                    result = (operant1 * operand2);
                    break;
                case "+":
                    //operator is addition
                    result = (operant1 + operand2);
                    break;
                case "-":
                    //operator is sub
                    result = (operant1 - operand2);
                    break;
                default:
                    MessageBox.Show("Enter one of the following valid operators \n +  -  *  \\", "Invalid Operator", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    break;
            }
            //print the result to the result textbox
            txtResult.Text = ("Result: " + result);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //close the app
            this.Close();
        }
    }
}
