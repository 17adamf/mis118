/*Class Name: frmSimpleCalc2
 * Author: Adam Flaigg-Fairless
 * Date: 10/25/18
 * Lab: Lab 5 Part 2
 * Class Description: Takes two numbers and a user-inputted operator and performs the calculation
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace simpleCalc2
{
    public partial class frmSimpleCalc2 : Form
    {
        public frmSimpleCalc2()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtResult.Enabled = false;
            nudNumber1.Text = "";
            nudNumber2.Text = "";
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            decimal number1 = nudNumber1.Value;
            decimal number2 = nudNumber2.Value;
            string operatorString = txtOperator.Text;

            if (
                IsPresent(nudNumber1, "Number 1") &&
                IsPresent(nudNumber2, "Number 2") &&
                IsOperator(txtOperator, "Operator") &&
                IsWithinValidRange(nudNumber1, "Number 1", 0, 1000000) &&
                IsWithinValidRange(nudNumber2, "Number 2", 0, 1000000)
                )
            {
                calculateResult(number1, number2, operatorString);
            }
            else
            {
                MessageBox.Show("Enter Valid Data Please", "Reenter Data");   
            }
            

        }

        private void calculateResult(decimal operant1, decimal operand2, string operatorString)
        {
            decimal result = 0;
            //determine the operator, run the calculation, and output to the textbox
            switch (operatorString)
            {
                case "/":
                case "\\":
                    //check both slashes incase user puts in a back slash
                    //operator is division
                    result = (operant1 / operand2);
                    break;
                case "*":
                    //operator is multi
                    result = (operant1 * operand2);
                    break;
                case "+":
                    //operator is addition
                    result = (operant1 + operand2);
                    break;
                case "-":
                    //operator is sub
                    result = (operant1 - operand2);
                    break;
                default:
                    MessageBox.Show("Somehow passed invalid operator", "Error");
                    break;
            }
            //print the result to the result textbox
            txtResult.Text = ("Result: " + result);
        }
        public bool IsPresent(NumericUpDown numericUpDown, string name)
        {
            if (numericUpDown.Value == 0)
            {
                MessageBox.Show(name + " is a required field.", "Entry Error");
                return false;
            }
            return true;
        }
        public bool IsDecimal(NumericUpDown numericUpDown, string name)
        {

            if (numericUpDown.Value is decimal)
            {
                return true;
            }
            else
            {
                MessageBox.Show(name + " must be a decimal value.", "Entry Error");
                return false;
            }
        }
        public bool IsOperator(TextBox txtoperator, string name)
        {
            if (txtOperator.Text == "+" || txtOperator.Text == "-" || txtOperator.Text == "*" || txtOperator.Text == "\\" || txtOperator.Text == "/")
            {
                return true;
            }
            else
            {
                MessageBox.Show("Enter one of the following valid operators \n +  -  *  \\", "Invalid Operator", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return false;
            }
        }
        public bool IsWithinValidRange(NumericUpDown numericUpDown, string name, int lowerLimit, int upperLimit)
        {
            if (numericUpDown.Value > lowerLimit && numericUpDown.Value < upperLimit)
            {

                return true;
            }
            else
            {
                MessageBox.Show(name + " is not within range", "Entry Error");
                return false;
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            //close the app
            this.Close();
        }
    }
}
