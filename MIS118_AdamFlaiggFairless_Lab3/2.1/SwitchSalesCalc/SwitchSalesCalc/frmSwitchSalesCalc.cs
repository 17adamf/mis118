/*Class Name: frmSwitchSalesCalc
 * Author: Adam Flaigg-Fairless
 * Date: 10/8/18
 * Lab: Lab 1 Part 2
 * Class Description: Adds item prices to a listbox based on user input and totals them upon user input
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwitchSalesCalc
{
    public partial class frmSwitchSalesCalc : Form
    {

        Int32 int32Quantity = 1;
        int intSelected = 0;
        double dubInputNumber = 0;
        double dubOutputNumber = 0;
        double dubCurrentTotal;
        string strTotal;

        public frmSwitchSalesCalc()
        {
            InitializeComponent();
        }

        private void frmSwitchSalesCalc_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //append items to the totals list
            updateTotalsList();
        }

        private void btnTotal_Click(object sender, EventArgs e)
        {
            //sum items in totals list
            outputTotal();
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            //reset all values and clear the form
            resetForm();
        }


		//any time a radio button is selected, check what was clicked and save it to global
        private void rbProduct1_CheckedChanged(object sender, EventArgs e)
        {
            updateSelected();
        }

        private void rbProduct2_CheckedChanged(object sender, EventArgs e)
        {
            updateSelected();
        }

        private void rbProduct3_CheckedChanged(object sender, EventArgs e)
        {
            updateSelected();
        }

        private void rbProduct4_CheckedChanged(object sender, EventArgs e)
        {
            updateSelected();
        }

        private void rbProduct5_CheckedChanged(object sender, EventArgs e)
        {
            updateSelected();
        }

        private void nudQuantity_ValueChanged(object sender, EventArgs e)
        {
			//any time the value is changed in  the quantity box, update it to the global
            int32Quantity = Convert.ToInt32(nudQuantity.Value);   
        }

        private void liboxTotals_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void updateSelected()
        {
            //check the selected radio button and save it to the global
            if (rbProduct1.Checked == true)
            {
                intSelected = 1;
            }
            else if (rbProduct2.Checked == true)
            {
                intSelected = 2;
            }
            else if (rbProduct3.Checked == true)
            {
                intSelected = 3;
            }
            else if (rbProduct4.Checked == true)
            {
                intSelected = 4;
            }
            else if (rbProduct5.Checked == true)
            {
                intSelected = 5;
            }
            else
            {
                intSelected = 0;
            }
        }


        private void updateTotalsList()
        {
            //switch for the different radio buttons, adds price to list based on listed price
            switch (intSelected)
            {
                case 1:
                    dubInputNumber = 3.99;
                    pushNumbers();
                    break;
                case 2:
                    dubInputNumber = 1.40;
                    pushNumbers();
                    break;
                case 3:
                    dubInputNumber = 2.10;
                    pushNumbers();
                    break;
                case 4:
                    dubInputNumber = 5.01;
                    pushNumbers();
                    break;
                case 5:
                    dubInputNumber = 1.98;
                    pushNumbers();
                    break;
                default:
                    MessageBox.Show("Make a Selection");
                    break;
            }

        }
        
        private void pushNumbers()
        {
            //do math and push to list
            dubOutputNumber = dubInputNumber * int32Quantity;
            liboxTotals.Items.Add(dubOutputNumber);

            //clear globals
            nudQuantity.Value = 1;
            int32Quantity = 1;
            dubInputNumber = 0;
            dubOutputNumber = 0;
        }

        private void outputTotal()
        {
            /*
             * This function goes through each item in the listbox, and adds the item to the
             * current total, removes the item, and moves on until there are no more values
             * in liboxTotals, where it then makes sure no items are left in the listbox,
             * totals the number, and pushes all the values to the total.
             * */


            //disable list box rendering to save on performance
            liboxTotals.BeginUpdate();
            try
            {
                //count through the list box
                for (int i = liboxTotals.Items.Count - 1; i >= 0; i--)
                {
                    //add the currently selected item to the current running total
                    dubCurrentTotal += Convert.ToDouble(liboxTotals.Items[i]);
                    //remove the currently selected item
                    liboxTotals.Items.RemoveAt(i);
                }
            }
            finally
            {
                //resume graphical updates
                liboxTotals.EndUpdate();
                //clear list to be sure
                liboxTotals.Items.Clear();

                //disable the buttons until form is reset
                btnAdd.Enabled = false;
                btnTotal.Enabled = false;

                //push the values to the output label and clear the strTotal
                strTotal = Convert.ToString(dubCurrentTotal);
                lblTotal.Text = ($"Total: {strTotal}");
                strTotal = "";
            }
        }

        private void resetForm()
        {
            //clear all globals, enable buttons
            nudQuantity.Value = 1;
            int32Quantity = 1;
            dubInputNumber = 0;
            dubOutputNumber = 0;
            dubCurrentTotal = 0;
            liboxTotals.Items.Clear();
            lblTotal.Text = "";
            strTotal = "";
            btnAdd.Enabled = true;
            btnTotal.Enabled = true;
        }

        /* Price List
         * Product 1: $3.99
         * Product 2: $1.40
         * Product 3: $2.10
         * Product 4: $5.01
         * Product 5: $1.98
         * */
    }
}
