namespace SwitchSalesCalc
{
    partial class frmSwitchSalesCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbProduct1 = new System.Windows.Forms.RadioButton();
            this.rbProduct5 = new System.Windows.Forms.RadioButton();
            this.rbProduct4 = new System.Windows.Forms.RadioButton();
            this.rbProduct3 = new System.Windows.Forms.RadioButton();
            this.rbProduct2 = new System.Windows.Forms.RadioButton();
            this.nudQuantity = new System.Windows.Forms.NumericUpDown();
            this.btnAdd = new System.Windows.Forms.Button();
            this.liboxTotals = new System.Windows.Forms.ListBox();
            this.btnTotal = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // rbProduct1
            // 
            this.rbProduct1.AutoSize = true;
            this.rbProduct1.Location = new System.Drawing.Point(40, 53);
            this.rbProduct1.Name = "rbProduct1";
            this.rbProduct1.Size = new System.Drawing.Size(104, 17);
            this.rbProduct1.TabIndex = 0;
            this.rbProduct1.TabStop = true;
            this.rbProduct1.Text = "Product 1: $3.99";
            this.rbProduct1.UseVisualStyleBackColor = true;
            this.rbProduct1.CheckedChanged += new System.EventHandler(this.rbProduct1_CheckedChanged);
            // 
            // rbProduct5
            // 
            this.rbProduct5.AutoSize = true;
            this.rbProduct5.Location = new System.Drawing.Point(40, 145);
            this.rbProduct5.Name = "rbProduct5";
            this.rbProduct5.Size = new System.Drawing.Size(104, 17);
            this.rbProduct5.TabIndex = 1;
            this.rbProduct5.TabStop = true;
            this.rbProduct5.Text = "Product 5: $1.98";
            this.rbProduct5.UseVisualStyleBackColor = true;
            this.rbProduct5.CheckedChanged += new System.EventHandler(this.rbProduct5_CheckedChanged);
            // 
            // rbProduct4
            // 
            this.rbProduct4.AutoSize = true;
            this.rbProduct4.Location = new System.Drawing.Point(40, 122);
            this.rbProduct4.Name = "rbProduct4";
            this.rbProduct4.Size = new System.Drawing.Size(104, 17);
            this.rbProduct4.TabIndex = 2;
            this.rbProduct4.TabStop = true;
            this.rbProduct4.Text = "Product 4: $5.01";
            this.rbProduct4.UseVisualStyleBackColor = true;
            this.rbProduct4.CheckedChanged += new System.EventHandler(this.rbProduct4_CheckedChanged);
            // 
            // rbProduct3
            // 
            this.rbProduct3.AutoSize = true;
            this.rbProduct3.Location = new System.Drawing.Point(40, 99);
            this.rbProduct3.Name = "rbProduct3";
            this.rbProduct3.Size = new System.Drawing.Size(104, 17);
            this.rbProduct3.TabIndex = 3;
            this.rbProduct3.TabStop = true;
            this.rbProduct3.Text = "Product 3: $2.10";
            this.rbProduct3.UseVisualStyleBackColor = true;
            this.rbProduct3.CheckedChanged += new System.EventHandler(this.rbProduct3_CheckedChanged);
            // 
            // rbProduct2
            // 
            this.rbProduct2.AutoSize = true;
            this.rbProduct2.Location = new System.Drawing.Point(40, 76);
            this.rbProduct2.Name = "rbProduct2";
            this.rbProduct2.Size = new System.Drawing.Size(104, 17);
            this.rbProduct2.TabIndex = 4;
            this.rbProduct2.TabStop = true;
            this.rbProduct2.Text = "Product 2: $1.40";
            this.rbProduct2.UseVisualStyleBackColor = true;
            this.rbProduct2.CheckedChanged += new System.EventHandler(this.rbProduct2_CheckedChanged);
            // 
            // nudQuantity
            // 
            this.nudQuantity.Location = new System.Drawing.Point(40, 197);
            this.nudQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudQuantity.Name = "nudQuantity";
            this.nudQuantity.Size = new System.Drawing.Size(120, 20);
            this.nudQuantity.TabIndex = 5;
            this.nudQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudQuantity.ValueChanged += new System.EventHandler(this.nudQuantity_ValueChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(184, 174);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Add to Total";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // liboxTotals
            // 
            this.liboxTotals.FormattingEnabled = true;
            this.liboxTotals.Location = new System.Drawing.Point(283, 28);
            this.liboxTotals.Name = "liboxTotals";
            this.liboxTotals.Size = new System.Drawing.Size(226, 199);
            this.liboxTotals.TabIndex = 7;
            this.liboxTotals.SelectedIndexChanged += new System.EventHandler(this.liboxTotals_SelectedIndexChanged);
            // 
            // btnTotal
            // 
            this.btnTotal.Location = new System.Drawing.Point(184, 197);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(75, 23);
            this.btnTotal.TabIndex = 8;
            this.btnTotal.Text = "Total";
            this.btnTotal.UseVisualStyleBackColor = true;
            this.btnTotal.Click += new System.EventHandler(this.btnTotal_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(184, 220);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 9;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(283, 234);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(10, 13);
            this.lblTotal.TabIndex = 10;
            this.lblTotal.Text = " ";
            // 
            // frmSwitchSalesCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 255);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnTotal);
            this.Controls.Add(this.liboxTotals);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.nudQuantity);
            this.Controls.Add(this.rbProduct2);
            this.Controls.Add(this.rbProduct3);
            this.Controls.Add(this.rbProduct4);
            this.Controls.Add(this.rbProduct5);
            this.Controls.Add(this.rbProduct1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmSwitchSalesCalc";
            this.Text = "Switch Sales";
            this.Load += new System.EventHandler(this.frmSwitchSalesCalc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbProduct1;
        private System.Windows.Forms.RadioButton rbProduct5;
        private System.Windows.Forms.RadioButton rbProduct4;
        private System.Windows.Forms.RadioButton rbProduct3;
        private System.Windows.Forms.RadioButton rbProduct2;
        private System.Windows.Forms.NumericUpDown nudQuantity;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox liboxTotals;
        private System.Windows.Forms.Button btnTotal;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblTotal;
    }
}

