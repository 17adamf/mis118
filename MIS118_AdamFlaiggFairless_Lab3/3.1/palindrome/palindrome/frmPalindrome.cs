/*Class Name: frmSwitchSalesCalc
 * Author: Adam Flaigg-Fairless
 * Date: 10/8/18
 * Lab: Lab 1 Part 3
 * Class Description: takes an input number and determines if it is a palindrome based on a modlus function
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace palindrome
{
    public partial class frmPalindrome : Form
    {

        int inputNumber;
        int tempNumber;
        int remainder;
        int reverse = 0;

        public frmPalindrome()
        {
            InitializeComponent();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            //update result upon clicking
            updateResult();
        }

        private void lblResult_Click(object sender, EventArgs e)
        {


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void nudInput_ValueChanged(object sender, EventArgs e)
        {
            
            
        }

        private void updateResult()
        {
            //take the input number and convert it to an int32
            inputNumber = Convert.ToInt32(nudInput.Value);

            //store the original input for later comparison
            tempNumber = inputNumber;

            //run this while it is greater than 0
            while (inputNumber > 0)
            {

                //set the remainder to be the remainder of the input 
                remainder = inputNumber % 10;
                //add the remainder to the reverse times 10
                reverse = reverse * 10 + remainder;
                //divide the input number by 10
                inputNumber /= 10;
            }
            if (tempNumber == reverse)
            {
                //send result to label
                lblResult.Text = ($"{tempNumber} is a palindrome");
            }
            else
            {
                //send result to label
                lblResult.Text = ($"{tempNumber} is not a palindrome");
            }
            //clear variables out after running
            remainder = 0;
            reverse = 0;
            inputNumber = 0;
            tempNumber = 0;
            nudInput.Value = 11111;

        }

    }
}
