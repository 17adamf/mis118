/*Class Name: frmBMICalc
 * Author: Adam Flaigg-Fairless
 * Date: 10/8/18
 * Lab: Lab 1 Part 1
 * Class Description: Calculates BMI based on height and weight entered, and returns string with result 
 */



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMICalc
{
    public partial class frmBMICalc : Form
    {

        decimal decHeight;
        decimal decWeight;
        double BMI;
        
        public frmBMICalc()
        {
            InitializeComponent();
        }
        private void nudHeight_ValueChanged(object sender, EventArgs e)
        {
        }
        private void nudWeight_ValueChanged(object sender, EventArgs e)
        {
        }
        private void btnCalc_Click(object sender, EventArgs e)
        {
            //run the calculate function on button click
            calculateBMI();
        }
        public void calculateBMI()
        {
            //grab the values in the input boxes and push them to the vars
            decHeight = nudHeight.Value;
            decWeight = nudWeight.Value;


            //the program checks that there is a valid height so we're not dividing by 0
            if (decHeight > 0)
            {
                BMI = Convert.ToDouble((decWeight * 703) / (decHeight * decHeight));
                if (BMI <= 18.5)
                {
                    //BMI is underweight
                    lblResult.Text = ($"Underweight: BMI {BMI}");
                }
                else if (BMI > 18.5 && BMI < 24.9)
                {
                    //BMI is normal
                    lblResult.Text = ($"Normal: BMI {BMI}");
                }
                else if (BMI > 24.9 && BMI < 29.9)
                {
                    //BMI is overweight
                    lblResult.Text = ($"Overweight: BMI {BMI}");
                }
                else if (BMI > 29.9)
                {
                    //BMI is obese
                    lblResult.Text = ($"Obese: BMI {BMI}");
                }
            }
            //prompt for a valid height
            else if (BMI <= 0)
            {
                MessageBox.Show("Enter a valid height");
            }

        }
    }
}
