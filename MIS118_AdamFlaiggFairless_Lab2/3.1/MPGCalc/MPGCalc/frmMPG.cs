using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace MPGCalc
{
    public partial class frmMPG : Form
    {
        //globals
        decimal deMilesDriven;
        decimal deFuelUsed;
        decimal deCurrentEntry;

        public frmMPG()
        {
            InitializeComponent();
        }

        private void nudMilesDriven_ValueChanged(object sender, EventArgs e)
        {
            //update global miles driven
            deMilesDriven = nudMilesDriven.Value;
        }

        private void nudFuelUsed_ValueChanged(object sender, EventArgs e)
        {
            //update global fuel used
            deFuelUsed = nudFuelUsed.Value;
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            //calculate mpg 
            //make sure we're not dividing by zero
            if (deFuelUsed > 0 || deFuelUsed < 0)
            {
                deCurrentEntry = Math.Round((deMilesDriven / deFuelUsed), 4);
                lboxResults.Items.Add(deCurrentEntry + " MPG");
            }
            else
            {
                MessageBox.Show("You must enter fuel used", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //reset all stored values
            deMilesDriven = 0;
            deFuelUsed = 0;
            deCurrentEntry = 0;
            nudFuelUsed.Value = 0;
            nudMilesDriven.Value = 0;
        }

        private void lboxResults_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
