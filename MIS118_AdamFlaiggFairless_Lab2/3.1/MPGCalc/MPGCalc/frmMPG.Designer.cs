namespace MPGCalc
{
    partial class frmMPG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLog = new System.Windows.Forms.Button();
            this.nudFuelUsed = new System.Windows.Forms.NumericUpDown();
            this.nudMilesDriven = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lboxResults = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudFuelUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMilesDriven)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(46, 64);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(118, 25);
            this.btnLog.TabIndex = 3;
            this.btnLog.Text = "Log";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // nudFuelUsed
            // 
            this.nudFuelUsed.Location = new System.Drawing.Point(113, 38);
            this.nudFuelUsed.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudFuelUsed.Name = "nudFuelUsed";
            this.nudFuelUsed.Size = new System.Drawing.Size(85, 20);
            this.nudFuelUsed.TabIndex = 2;
            this.nudFuelUsed.ValueChanged += new System.EventHandler(this.nudFuelUsed_ValueChanged);
            // 
            // nudMilesDriven
            // 
            this.nudMilesDriven.Location = new System.Drawing.Point(113, 12);
            this.nudMilesDriven.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudMilesDriven.Name = "nudMilesDriven";
            this.nudMilesDriven.Size = new System.Drawing.Size(85, 20);
            this.nudMilesDriven.TabIndex = 1;
            this.nudMilesDriven.ValueChanged += new System.EventHandler(this.nudMilesDriven_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Miles Driven";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Fuel Used (gallons)";
            // 
            // lboxResults
            // 
            this.lboxResults.FormattingEnabled = true;
            this.lboxResults.Location = new System.Drawing.Point(204, 12);
            this.lboxResults.Name = "lboxResults";
            this.lboxResults.Size = new System.Drawing.Size(145, 264);
            this.lboxResults.TabIndex = 5;
            this.lboxResults.SelectedIndexChanged += new System.EventHandler(this.lboxResults_SelectedIndexChanged);
            // 
            // frmMPG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 292);
            this.Controls.Add(this.lboxResults);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudMilesDriven);
            this.Controls.Add(this.nudFuelUsed);
            this.Controls.Add(this.btnLog);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmMPG";
            this.Text = "MPG Calculator";
            ((System.ComponentModel.ISupportInitialize)(this.nudFuelUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMilesDriven)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.NumericUpDown nudFuelUsed;
        private System.Windows.Forms.NumericUpDown nudMilesDriven;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lboxResults;
    }
}

