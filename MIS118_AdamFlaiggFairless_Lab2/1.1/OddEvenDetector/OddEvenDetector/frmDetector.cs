using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OddEvenDetector
{
    public partial class frmDetector : Form
    {

        int inputNumber;


        public frmDetector()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

		//called when button is clicked
        private void btnCalc_Click(object sender, EventArgs e)
        {
			//take input number and convert it to int with no decimals
           inputNumber = Convert.ToInt32(nudInput.Value);
            if (inputNumber%2 == 0)
            {
                //number is even
                lblAnswer.ForeColor = Color.Green;
                lblAnswer.Text = ($"Number {inputNumber} is even");
            }
            else
            {
                //number is odd
                lblAnswer.ForeColor = Color.Red;
                lblAnswer.Text = ($"Number {inputNumber} is odd");

            }
        }

        private void nudInput_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lblAnswer_Click(object sender, EventArgs e)
        {

        }
    }
}
