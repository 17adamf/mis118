using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicCalculator
{
    public partial class frmCalc : Form
    {
        //define globals
        decimal decFirstNumber;
        decimal decSecondNumber;
        decimal valAdd;
        decimal valMinus;
        decimal valMulti;
        decimal valDiv;

        public frmCalc()
        {
            InitializeComponent();
        }

        private void frmCalc_Load(object sender, EventArgs e)
        {

        }

        //first value changed
        private void nudFirstNumber_ValueChanged(object sender, EventArgs e)
        {
            //update decimal global on change of input
            decFirstNumber = Convert.ToDecimal(nudFirstNumber.Value);
        }

        //second value changed
        private void nudSecondNumber_ValueChanged(object sender, EventArgs e)
        {
            //update decimal global on change of input
            decSecondNumber = Convert.ToDecimal(nudSecondNumber.Value);
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            //clears last input
            txtAnswer.Text = "";
            //does the calculation
            valAdd = (decFirstNumber + decSecondNumber);
            //write calculation to the text box
            txtAnswer.Text = valAdd.ToString();
            //clears the value
            valAdd = 0;

            //does this for each operator button clicked, but with different calculation
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            //see btnAdd_Click()
            txtAnswer.Text = "";
            valMinus = (decFirstNumber - decSecondNumber);
            txtAnswer.Text = valMinus.ToString();
            valMinus = 0;
        }

        private void btnMulti_Click(object sender, EventArgs e)
        {
            //see btnAdd_Click()
            txtAnswer.Text = "";
            valMulti = (decFirstNumber * decSecondNumber);
            txtAnswer.Text = valMulti.ToString();
            valMulti = 0;
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            //because you can't divide by 0, the program tests if the divisor aka decSecondNumber number is anything other than 0
            //if it isn't a 0, then we do the same basic operation as before
            if (decSecondNumber > 0 || decSecondNumber < 0)
            {
                //see btnAdd_Click()
                txtAnswer.Text = "";
                valDiv = (decFirstNumber / decSecondNumber);
                txtAnswer.Text = valDiv.ToString();
                valDiv = 0;
            }
            //if the divisor is a 0, the user is informed that they can not divide by 0, and the user must make a different selection or change the divisor
            else
            {
                MessageBox.Show("You can not divide by zero");
            }
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            //resets all values stored in the program
            decFirstNumber = 0;
            decSecondNumber = 0;
            valAdd = 0;
            valMinus = 0;
            valMulti = 0;
            valDiv = 0;
            txtAnswer.Text = "";
            nudFirstNumber.Value = 0;
            nudSecondNumber.Value = 0;
        }
    }
}
