using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SumAvgProd
{
    public partial class frmSumAvg : Form
    {
        //declare global numbers
        Int32 int32FirstNumber;//first number entered
        Int32 int32SecondNumber;//second number entered
        int valSum;//sum
        int valAvg;//average
        int valProd;//product


        public frmSumAvg()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void nudFirstNumber_ValueChanged(object sender, EventArgs e)
        {
            //convert number to int32 when number has changed and update results
            int32FirstNumber = Convert.ToInt32(nudFirstNumber.Value);
            updateResults();
        }

        private void nudSecondNumber_ValueChanged(object sender, EventArgs e)
        {
            //convert number to int32 when number has changed and update results
            int32SecondNumber = Convert.ToInt32(nudSecondNumber.Value);
            updateResults();
        }

        private void updateResults()
        {
            /*
             * Takes the first and second int and pushes them through the math in order to 
             * display the results in real time on the screen
             */

            //calculate the sum
            valSum = (int32FirstNumber + int32SecondNumber);
            lblSum.Text = ($"Sum of Numbers: {valSum}");
            //calculate the average
            valAvg = (int32FirstNumber + int32SecondNumber) / 2;
            lblAvg.Text = ($"Average of Numbers: {valAvg}");
            //calculate the product
            valProd = (int32FirstNumber * int32SecondNumber);
            lblProd.Text = ($"Product of Numbers: {valProd}");
            //detect which number is bigger
            if (int32FirstNumber > int32SecondNumber)
            {
                lblLargeSmall.Text = ($"Largest and Smallest Numbers (Respectively): {int32FirstNumber} , {int32SecondNumber}");
            }
            else if (int32FirstNumber < int32SecondNumber)
            {
                lblLargeSmall.Text = ($"Largest and Smallest Numbers (Respectively): {int32SecondNumber} , {int32FirstNumber}");
            }
            else if (int32FirstNumber == int32SecondNumber)
            {
                lblLargeSmall.Text = ("Numbers are Equal");
            }
            else
            {
                lblLargeSmall.Text = ("This should never appear on the screen");
            }
        }

        private void lblFirstNumber_Click(object sender, EventArgs e)
        {

        }
    }
}
