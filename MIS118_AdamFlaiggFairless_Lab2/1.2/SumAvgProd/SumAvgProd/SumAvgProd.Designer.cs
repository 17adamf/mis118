namespace SumAvgProd
{
    partial class frmSumAvg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudFirstNumber = new System.Windows.Forms.NumericUpDown();
            this.nudSecondNumber = new System.Windows.Forms.NumericUpDown();
            this.lblSum = new System.Windows.Forms.Label();
            this.lblAvg = new System.Windows.Forms.Label();
            this.lblProd = new System.Windows.Forms.Label();
            this.lblLargeSmall = new System.Windows.Forms.Label();
            this.lblFirstNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudFirstNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSecondNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // nudFirstNumber
            // 
            this.nudFirstNumber.Location = new System.Drawing.Point(201, 21);
            this.nudFirstNumber.Name = "nudFirstNumber";
            this.nudFirstNumber.Size = new System.Drawing.Size(120, 20);
            this.nudFirstNumber.TabIndex = 0;
            this.nudFirstNumber.ValueChanged += new System.EventHandler(this.nudFirstNumber_ValueChanged);
            // 
            // nudSecondNumber
            // 
            this.nudSecondNumber.Location = new System.Drawing.Point(201, 72);
            this.nudSecondNumber.Name = "nudSecondNumber";
            this.nudSecondNumber.Size = new System.Drawing.Size(120, 20);
            this.nudSecondNumber.TabIndex = 1;
            this.nudSecondNumber.ValueChanged += new System.EventHandler(this.nudSecondNumber_ValueChanged);
            // 
            // lblSum
            // 
            this.lblSum.AutoSize = true;
            this.lblSum.Location = new System.Drawing.Point(52, 153);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(88, 13);
            this.lblSum.TabIndex = 2;
            this.lblSum.Text = "Sum of Numbers:";
            // 
            // lblAvg
            // 
            this.lblAvg.AutoSize = true;
            this.lblAvg.Location = new System.Drawing.Point(52, 180);
            this.lblAvg.Name = "lblAvg";
            this.lblAvg.Size = new System.Drawing.Size(107, 13);
            this.lblAvg.TabIndex = 3;
            this.lblAvg.Text = "Average of Numbers:";
            // 
            // lblProd
            // 
            this.lblProd.AutoSize = true;
            this.lblProd.Location = new System.Drawing.Point(52, 208);
            this.lblProd.Name = "lblProd";
            this.lblProd.Size = new System.Drawing.Size(104, 13);
            this.lblProd.TabIndex = 4;
            this.lblProd.Text = "Product of Numbers:";
            // 
            // lblLargeSmall
            // 
            this.lblLargeSmall.AutoSize = true;
            this.lblLargeSmall.Location = new System.Drawing.Point(52, 235);
            this.lblLargeSmall.Name = "lblLargeSmall";
            this.lblLargeSmall.Size = new System.Drawing.Size(223, 13);
            this.lblLargeSmall.TabIndex = 5;
            this.lblLargeSmall.Text = "Largest and Smallest Numbers (Respectively):";
            // 
            // lblFirstNumber
            // 
            this.lblFirstNumber.AutoSize = true;
            this.lblFirstNumber.Location = new System.Drawing.Point(88, 28);
            this.lblFirstNumber.Name = "lblFirstNumber";
            this.lblFirstNumber.Size = new System.Drawing.Size(66, 13);
            this.lblFirstNumber.TabIndex = 6;
            this.lblFirstNumber.Text = "First Number";
            this.lblFirstNumber.Click += new System.EventHandler(this.lblFirstNumber_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Second Number";
            // 
            // frmSumAvg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblFirstNumber);
            this.Controls.Add(this.lblLargeSmall);
            this.Controls.Add(this.lblProd);
            this.Controls.Add(this.lblAvg);
            this.Controls.Add(this.lblSum);
            this.Controls.Add(this.nudSecondNumber);
            this.Controls.Add(this.nudFirstNumber);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmSumAvg";
            this.Text = "Sum, Average, Product, Etc. ";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudFirstNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSecondNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudFirstNumber;
        private System.Windows.Forms.NumericUpDown nudSecondNumber;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label lblAvg;
        private System.Windows.Forms.Label lblProd;
        private System.Windows.Forms.Label lblLargeSmall;
        private System.Windows.Forms.Label lblFirstNumber;
        private System.Windows.Forms.Label label1;
    }
}

