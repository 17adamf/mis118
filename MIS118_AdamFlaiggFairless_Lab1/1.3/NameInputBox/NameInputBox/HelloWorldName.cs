using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace NameInputBox
{
    public partial class HelloWorldName : Form
    {
        string strname;

        public HelloWorldName()
        {
            InitializeComponent();
        }
        private void textBoxInput_TextChanged(object sender, EventArgs e)
        {
            strname = textBoxInput.Text;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            helloOutput.Text = $"Hello World, My Name is: {strname}";
            textBoxInput.Text = "";
        }
    }
}
