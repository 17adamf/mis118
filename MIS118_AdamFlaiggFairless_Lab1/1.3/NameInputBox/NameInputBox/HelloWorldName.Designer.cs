namespace NameInputBox
{
    partial class HelloWorldName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.whatisyourname = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            this.helloOutput = new System.Windows.Forms.Label();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // whatisyourname
            // 
            this.whatisyourname.AutoSize = true;
            this.whatisyourname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whatisyourname.Location = new System.Drawing.Point(66, 103);
            this.whatisyourname.Name = "whatisyourname";
            this.whatisyourname.Size = new System.Drawing.Size(149, 20);
            this.whatisyourname.TabIndex = 0;
            this.whatisyourname.Text = "What is your name?";
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(408, 103);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 1;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // helloOutput
            // 
            this.helloOutput.AutoSize = true;
            this.helloOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helloOutput.Location = new System.Drawing.Point(66, 211);
            this.helloOutput.Name = "helloOutput";
            this.helloOutput.Size = new System.Drawing.Size(0, 20);
            this.helloOutput.TabIndex = 2;
            // 
            // textBoxInput
            // 
            this.textBoxInput.Location = new System.Drawing.Point(241, 105);
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.Size = new System.Drawing.Size(148, 20);
            this.textBoxInput.TabIndex = 3;
            this.textBoxInput.TextChanged += new System.EventHandler(this.textBoxInput_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.textBoxInput);
            this.Controls.Add(this.helloOutput);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.whatisyourname);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Hello World";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label whatisyourname;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label helloOutput;
        private System.Windows.Forms.TextBox textBoxInput;
    }
}

