/*Class Name: frmDateTest
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 1
 * Class Description: gives the user a button to click that will display the date
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace datedemo
{
    public partial class frmDateTest : Form
    {
        //create a new date class with the current date
        Date date = new Date(System.DateTime.Today.Month,
                             System.DateTime.Today.Day,
                             System.DateTime.Today.Year);
        public frmDateTest()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnDisplayDate_Click(object sender, EventArgs e)
        {
            //show the date using the date class
            MessageBox.Show(date.DisplayDate());
        }
    }
}
