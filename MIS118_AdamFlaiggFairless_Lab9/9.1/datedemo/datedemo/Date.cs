﻿/*Class Name: Date
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 1
 * Class Description: Class that is created with a month day and year, and can return a formatted string with the supplied date
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace datedemo
{
    class Date
    {
        //create the ints for the month day and year
        public int month { get; set; }
        public int day { get; set; }
        public int year { get; set; }
        //create a datetime to be filled in later
        DateTime suppliedDate;

        public Date(int setmonth, int setday, int setyear)
        {
            //set the date from the supplied date
            month = setmonth;
            day = setday;
            year = setyear;
            //fill in the datetime
            suppliedDate = new DateTime(year, month, day);
        }
        public string DisplayDate()
        {
            //return a string with the supplieddate
            return suppliedDate.ToString("MM/dd/yyyy");
        }
    }
}
