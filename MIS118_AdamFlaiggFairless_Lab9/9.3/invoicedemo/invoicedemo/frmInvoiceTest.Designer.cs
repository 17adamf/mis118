namespace invoicedemo
{
    partial class frmInvoiceTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPartNumber = new System.Windows.Forms.TextBox();
            this.txtPartDesc = new System.Windows.Forms.TextBox();
            this.nudPartQuant = new System.Windows.Forms.NumericUpDown();
            this.nudPartPrice = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDisplay = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudPartQuant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPartPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPartNumber
            // 
            this.txtPartNumber.Location = new System.Drawing.Point(48, 19);
            this.txtPartNumber.Name = "txtPartNumber";
            this.txtPartNumber.Size = new System.Drawing.Size(100, 20);
            this.txtPartNumber.TabIndex = 0;
            this.txtPartNumber.Text = "Part No.";
            // 
            // txtPartDesc
            // 
            this.txtPartDesc.Location = new System.Drawing.Point(47, 45);
            this.txtPartDesc.Name = "txtPartDesc";
            this.txtPartDesc.Size = new System.Drawing.Size(100, 20);
            this.txtPartDesc.TabIndex = 1;
            this.txtPartDesc.Text = "Part Desc.";
            // 
            // nudPartQuant
            // 
            this.nudPartQuant.Location = new System.Drawing.Point(48, 72);
            this.nudPartQuant.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudPartQuant.Name = "nudPartQuant";
            this.nudPartQuant.Size = new System.Drawing.Size(99, 20);
            this.nudPartQuant.TabIndex = 2;
            // 
            // nudPartPrice
            // 
            this.nudPartPrice.Location = new System.Drawing.Point(47, 98);
            this.nudPartPrice.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudPartPrice.Name = "nudPartPrice";
            this.nudPartPrice.Size = new System.Drawing.Size(99, 20);
            this.nudPartPrice.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Quantity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Price";
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(47, 125);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(151, 23);
            this.btnDisplay.TabIndex = 6;
            this.btnDisplay.Text = "Display Item";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // frmInvoiceTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 179);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudPartPrice);
            this.Controls.Add(this.nudPartQuant);
            this.Controls.Add(this.txtPartDesc);
            this.Controls.Add(this.txtPartNumber);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmInvoiceTest";
            this.Text = "Invoice Demo";
            ((System.ComponentModel.ISupportInitialize)(this.nudPartQuant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPartPrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPartNumber;
        private System.Windows.Forms.TextBox txtPartDesc;
        private System.Windows.Forms.NumericUpDown nudPartQuant;
        private System.Windows.Forms.NumericUpDown nudPartPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDisplay;
    }
}

