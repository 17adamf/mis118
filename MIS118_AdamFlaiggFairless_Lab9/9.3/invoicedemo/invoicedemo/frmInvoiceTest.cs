/*Class Name: frmInvoiceTest
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 3
 * Class Description: Gives the user a form to fill out that will display an item invoice
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace invoicedemo
{
    public partial class frmInvoiceTest : Form
    {
        public frmInvoiceTest()
        {
            InitializeComponent();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            //create a new instance of Invoice and pass it the user entered data, then show the invoice
            Invoice invoice = new Invoice(txtPartNumber.Text, txtPartDesc.Text, Convert.ToInt32(nudPartQuant.Value), nudPartPrice.Value);
            invoice.ShowWholeInvoice();
        }
    }
}
