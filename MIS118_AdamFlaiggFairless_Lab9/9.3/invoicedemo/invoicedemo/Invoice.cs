﻿/*Class Name: Invoice
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 3
 * Class Description: Class that is created with a month day and year, and can return a formatted string with the supplied date
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace invoicedemo
{
    class Invoice
    {
        //create the variables
        public string partNumber { get; set; }
        public string partDesc { get; set; }
        public int partQuant { get; set; }
        public decimal partPrice { get; set; }


        public Invoice(string setPartNumber, string setPartDesc, int setPartQuant, decimal setpartPrice)
        {
            //pass the variables provided into the local variables
            partNumber = setPartNumber;
            partDesc = setPartDesc;
            partQuant = setPartQuant;
            partPrice = setpartPrice;
        }
        public decimal GetInvoiceAmount()
        {
            //return the price times the quantity
            return partPrice * partQuant;
        }
        public void ShowWholeInvoice()
        {
            //display all of the information from the invoice as a message box
            MessageBox.Show("Part Number: " + partNumber + "\nPart Desc.:" + partDesc + "\nPart Quantity: " + partQuant.ToString() + "\nPart Price: " + partPrice.ToString() + "\n\nPrice Total: " + GetInvoiceAmount(), "Invoice Item");
        }
    }
}
