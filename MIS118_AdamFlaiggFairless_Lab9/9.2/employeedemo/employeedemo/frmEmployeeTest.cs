/*Class Name: frmEmployeeTest
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 2
 * Class Description: gives the user the ability to create two employees using the Employee class
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace employeedemo
{
    public partial class frmEmployeeTest : Form
    {
        public frmEmployeeTest()
        {
            InitializeComponent();
        }

        private void btnCreateEmp1_Click(object sender, EventArgs e)
        {
            //create a new instance of Employee
            Employee emp1 = new Employee(txtEmpFName1.Text, txtEmpLName1.Text, Convert.ToDecimal(nudMonthlySal1.Value));
            //show the employee from the created instance
            emp1.DisplayEmp();
        }

        private void btnCreateEmp2_Click(object sender, EventArgs e)
        {
            //create a new instance of Employee
            Employee emp2 = new Employee(txtEmpFName1.Text, txtEmpLName1.Text, Convert.ToDecimal(nudMonthlySal1.Value));
            //show the employee from the created instance
            emp2.DisplayEmp();
        }
    }
}
