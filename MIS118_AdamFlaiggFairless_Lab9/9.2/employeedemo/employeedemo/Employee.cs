﻿/*Class Name: Employee
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 2
 * Class Description: creates an employee with a first and last name, and monthly salary. Can display that info in yearly format
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace employeedemo
{
    class Employee
    {
        //create the vars for the employee
        public string empFName { get; set; }
        public string empLName { get; set; }
        public decimal empMonthySal { get; set; }
        
        public Employee(string setFName, string setLName, decimal setMonthlySal)
        {
            //set the vars to be equal to the supplied values
            empFName = setFName;
            empLName = setLName;
            empMonthySal = setMonthlySal;

        }

        public string GetYearlyString()
        {
            //calculate the yearly salary and create a string with that information
            decimal yearlySal = empMonthySal * 12;
            return (empLName + ", " + empFName + "\nYearly Salary: " + yearlySal.ToString());
        }
        public void DisplayEmp()
        {
            //displays the employee's yearly salary. this method is mostly for consistency 
            MessageBox.Show(GetYearlyString(), empFName + "'s Yearly Salary");
        }
    }
}
