namespace employeedemo
{
    partial class frmEmployeeTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEmpFName1 = new System.Windows.Forms.TextBox();
            this.txtEmpLName1 = new System.Windows.Forms.TextBox();
            this.nudMonthlySal1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreateEmp1 = new System.Windows.Forms.Button();
            this.btnCreateEmp2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.nudMonthlySal2 = new System.Windows.Forms.NumericUpDown();
            this.txtEmpLName2 = new System.Windows.Forms.TextBox();
            this.txtEmpFName2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthlySal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthlySal2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtEmpFName1
            // 
            this.txtEmpFName1.Location = new System.Drawing.Point(13, 13);
            this.txtEmpFName1.Name = "txtEmpFName1";
            this.txtEmpFName1.Size = new System.Drawing.Size(100, 20);
            this.txtEmpFName1.TabIndex = 0;
            this.txtEmpFName1.Text = "First Name";
            // 
            // txtEmpLName1
            // 
            this.txtEmpLName1.Location = new System.Drawing.Point(119, 12);
            this.txtEmpLName1.Name = "txtEmpLName1";
            this.txtEmpLName1.Size = new System.Drawing.Size(100, 20);
            this.txtEmpLName1.TabIndex = 1;
            this.txtEmpLName1.Text = "Last Name";
            // 
            // nudMonthlySal1
            // 
            this.nudMonthlySal1.Location = new System.Drawing.Point(13, 39);
            this.nudMonthlySal1.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudMonthlySal1.Name = "nudMonthlySal1";
            this.nudMonthlySal1.Size = new System.Drawing.Size(100, 20);
            this.nudMonthlySal1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Monthly Salary";
            // 
            // btnCreateEmp1
            // 
            this.btnCreateEmp1.Location = new System.Drawing.Point(13, 66);
            this.btnCreateEmp1.Name = "btnCreateEmp1";
            this.btnCreateEmp1.Size = new System.Drawing.Size(206, 23);
            this.btnCreateEmp1.TabIndex = 4;
            this.btnCreateEmp1.Text = "Create Employee 1";
            this.btnCreateEmp1.UseVisualStyleBackColor = true;
            this.btnCreateEmp1.Click += new System.EventHandler(this.btnCreateEmp1_Click);
            // 
            // btnCreateEmp2
            // 
            this.btnCreateEmp2.Location = new System.Drawing.Point(13, 180);
            this.btnCreateEmp2.Name = "btnCreateEmp2";
            this.btnCreateEmp2.Size = new System.Drawing.Size(206, 23);
            this.btnCreateEmp2.TabIndex = 9;
            this.btnCreateEmp2.Text = "Create Employee 1";
            this.btnCreateEmp2.UseVisualStyleBackColor = true;
            this.btnCreateEmp2.Click += new System.EventHandler(this.btnCreateEmp2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Monthly Salary";
            // 
            // nudMonthlySal2
            // 
            this.nudMonthlySal2.Location = new System.Drawing.Point(13, 153);
            this.nudMonthlySal2.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudMonthlySal2.Name = "nudMonthlySal2";
            this.nudMonthlySal2.Size = new System.Drawing.Size(100, 20);
            this.nudMonthlySal2.TabIndex = 7;
            // 
            // txtEmpLName2
            // 
            this.txtEmpLName2.Location = new System.Drawing.Point(119, 126);
            this.txtEmpLName2.Name = "txtEmpLName2";
            this.txtEmpLName2.Size = new System.Drawing.Size(100, 20);
            this.txtEmpLName2.TabIndex = 6;
            this.txtEmpLName2.Text = "Last Name";
            // 
            // txtEmpFName2
            // 
            this.txtEmpFName2.Location = new System.Drawing.Point(13, 127);
            this.txtEmpFName2.Name = "txtEmpFName2";
            this.txtEmpFName2.Size = new System.Drawing.Size(100, 20);
            this.txtEmpFName2.TabIndex = 5;
            this.txtEmpFName2.Text = "First Name";
            // 
            // frmEmployeeTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 237);
            this.Controls.Add(this.btnCreateEmp2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudMonthlySal2);
            this.Controls.Add(this.txtEmpLName2);
            this.Controls.Add(this.txtEmpFName2);
            this.Controls.Add(this.btnCreateEmp1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudMonthlySal1);
            this.Controls.Add(this.txtEmpLName1);
            this.Controls.Add(this.txtEmpFName1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmEmployeeTest";
            this.Text = "Employee Test";
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthlySal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthlySal2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEmpFName1;
        private System.Windows.Forms.TextBox txtEmpLName1;
        private System.Windows.Forms.NumericUpDown nudMonthlySal1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreateEmp1;
        private System.Windows.Forms.Button btnCreateEmp2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudMonthlySal2;
        private System.Windows.Forms.TextBox txtEmpLName2;
        private System.Windows.Forms.TextBox txtEmpFName2;
    }
}

