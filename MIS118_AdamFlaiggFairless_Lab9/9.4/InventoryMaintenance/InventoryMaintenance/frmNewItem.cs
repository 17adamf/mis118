﻿/*Class Name: frmNewItem
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 4
 * Class Description: Prompts the user for a new item to add to the inventory
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryMaintenance
{
    public partial class frmNewItem : Form
    {
        public frmNewItem()
        {
            InitializeComponent();
        }

        // Add a statement here that declares the inventory item.
        InvItem tempItem = null;

        // Add a method here that gets and returns a new item.
        public string GetNewItem()
        {
            return tempItem.GetDisplayText();
        }
        public InvItem GetInvItem()
        {
            return tempItem.returnItem();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValidData())
            {
                // Add code here that creates a new item

                tempItem = new InvItem(Convert.ToInt32(txtItemNo.Text), Convert.ToDecimal(txtPrice.Text), txtDescription.Text);
                // and closes the form.
                Close();
            }
        }

        private bool IsValidData()
        {
            return Validator.IsPresent(txtItemNo) &&
                   Validator.IsInt32(txtItemNo) &&
                   Validator.IsPresent(txtDescription) &&
                   Validator.IsPresent(txtPrice) &&
                   Validator.IsDecimal(txtPrice);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
