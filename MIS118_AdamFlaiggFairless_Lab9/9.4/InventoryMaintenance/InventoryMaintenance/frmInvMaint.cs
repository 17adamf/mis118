/*Class Name: frmInvMaint
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 4
 * Class Description: Allows the user to look at the inventory and add and remove items from said inventory
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryMaintenance
{
    public partial class frmInvMaint : Form
    {
        public frmInvMaint()
        {
            InitializeComponent();
        }
        // Add a statement here that declares the list of items.
        List<InvItem> invItems = null;
        public int selectedIndex = 0;

        private void frmInvMaint_Load(object sender, EventArgs e)
        {
            // Add a statement here that gets the list of items.
            FillItemListBox();
        }
        private void FillItemListBox()
        {
            lstItems.Items.Clear();
            // Add code here that loads the list box with the items in the list.
            invItems = InvItemDB.GetItems();
            foreach (InvItem item in invItems)
            {
                InvItem tempItem = new InvItem(item.ItemNo, item.Price, item.Description);
                lstItems.Items.Add(tempItem.GetDisplayText());
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            // Add code here that creates an instance of the New Item form
            frmNewItem frmNewItem = new frmNewItem();
            frmNewItem.ShowDialog();
            // and then gets a new item from that form.
            lstItems.Items.Add(frmNewItem.GetNewItem());
            invItems.Add(frmNewItem.GetInvItem());
            InvItemDB.SaveItems(invItems);
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Add code here that displays a dialog box to confirm
            DialogResult result = MessageBox.Show("Are you sure you want to delete this item?\n" + lstItems.SelectedItem.ToString(), "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            selectedIndex = lstItems.SelectedIndex;
            if (result == DialogResult.Yes)
            {
                // the deletion and then removes the item from the list,
                // saves the list of products, and refreshes the list box
                // if the deletion is confirmed.
                lstItems.Items.RemoveAt(selectedIndex);
                invItems.RemoveAt(selectedIndex);
                InvItemDB.SaveItems(invItems);
                FillItemListBox();
            }

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}