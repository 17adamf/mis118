﻿/*Class Name: InvItem
 * Author: Adam Flaigg-Fairless
 * Date: 11/29/2018
 * Lab: Lab 9 Part 4
 * Class Description: Class for creating the InvItem type
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryMaintenance
{
    public class InvItem
    {
        public int ItemNo { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public InvItem(int SetItemNo = 0, decimal SetPrice = 0, string SetDescription = "")
        {
            ItemNo = SetItemNo;
            Price = SetPrice;
            Description = SetDescription;
        }
        public string GetDisplayText()
        {
            return (ItemNo.ToString() + "    " + Description + " ($" + Price + ")");
        }
        public InvItem returnItem()
        {
            InvItem tempItem = new InvItem(ItemNo, Price, Description);
            return tempItem;
        }
    }
}
